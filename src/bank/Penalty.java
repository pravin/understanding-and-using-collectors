
package bank;

import java.io.Serializable;

public interface Penalty extends Serializable {
    public long compute(long bal, long minBal);
    public static final Penalty DEFAULT = fixed(100);
    public static Penalty fixed(long fp) {
        return (bal, minBal) -> bal>=minBal?0:fp;
    }
    public static Penalty percent(int per) {
        return (bal, minBal) -> bal>=minBal?0:(minBal-bal)*per/100;
    }
}

