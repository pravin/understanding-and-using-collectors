This sample code is for use by the attendees of the session on "Understanding and Using Collectors" conducted by Bangalore Java User Group on 12th December 2020.
This code will be used to give examples while explaining the topic.
This sample code contains a base class called Account with two sub-classes CurrentAccount and SavingsAccount.
The Account class has an inner class called Transaction, and is managing a List<Transaction>.
The code also contains a class called Bank, which in turn manages a Map<Long,Account> to manage the Objects.
The method names are self explanatory.

You are free to tweak and use this code in other training sessions.

Enjoy!
